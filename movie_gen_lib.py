import subprocess as sp
import os
import imageio
import imageio_ffmpeg
from PIL import Image, ImageDraw, ImageFont
from moviepy.editor import *
from pathlib import Path
import numpy as np
import audiofile as af
import ffmpeg
from os import path
import pydub
from pydub import AudioSegment



def name_drawer(planet, artistname="Placeholder"):
    im = Image.open("templates/%s.jpg" % (planet))
    draw = ImageDraw.Draw(im)

    #create Fonts
    fontsFolder = '/fonts'  # e.g. ‘/Library/Fonts'
    HelveticaNeueFont = ImageFont.truetype("fonts/HelveticaNeue.otf", 70)
    HelveticaNeueUltraLightFont = ImageFont.truetype(os.path.join(fontsFolder, 'HelveticaNeue-UltraLight.otf'), 70)
    HelveticaNeueLightFont = ImageFont.truetype(os.path.join(fontsFolder, 'HelveticaNeue-Light.otf'), 70)

    #message
    msg = "%s\nby\n%s" % (planet, artistname)

    #draw
    w, h = draw.textsize(msg, font=HelveticaNeueUltraLightFont)
    draw.multiline_text(((1920 - w)/2, (1080 - h)/2), msg, fill="white", align="center", spacing=4, font=HelveticaNeueUltraLightFont)

    return np.array(im)

def audio_fixer(audio_in):

    pydub.AudioSegment.converter = r"C:\ProgramData\Anaconda3\Library\bin\ffmpeg.exe"
    if audio_in.find(".mp3") != -1:

        sound = AudioSegment.from_mp3(audio_in)
        new_filepath = audio_in.replace(".mp3", ".wav")
        sound.export(new_filepath, format="wav")
    else:
        new_filepath = audio_in

    return new_filepath

def vid_maker(planet, artistname, audio_in, framerate):
    #make background image
    img = name_drawer(planet, artistname)

    #fix_audio
    audio_in = audio_fixer(audio_in)
    dur = af.duration(audio_in)

    #audio visualization
    clips = []

    #set audio
    clip = ImageClip(img, duration=dur)
    clip_w_audio = clip.set_audio(AudioFileClip(audio_in))

    #make output name
    outputname = "%s_%s.avi" % (artistname, planet)

    #render final video
    clip_w_audio.set_duration(dur).write_videofile(outputname, fps=framerate, codec="libx264")


    # audio_vis_w_audio = audio_vis.set_audio(AudioFileClip(audio_in))
    # audio_vis_w_audio.write_videofile(outputname, fps=framerate, codec="libx264")

vid_maker("Saturn", "Test Name", )